import roboticstoolbox as rtb
from spatialmath import SE3
from spatialmath.pose3d import SO3
from spatialmath.base import transforms3d as t3d
from scipy import optimize as opt
from scipy import linalg as lin

# cost function
def pose_error_cost(q, rob, Tdes, w0, qtarget):

    # compute the current pose 
    T = rob.fkine(q)
    Tcurr = SE3(T)
    # print(Tcurr.R.transpose())
    # print(Tdes.R)
 
    # compute the rotation error as a rotation matrix
    Rerror = SO3((Tcurr.R.transpose()))*Tdes.R

    [omega, theta] = t3d.tr2angvec(Rerror)
    orientation_error = ((theta*omega)**2).sum()
    
    position_error = (((Tdes-Tcurr)[0:3,3])**2).sum()

    print(orientation_error+position_error+ w0*((qtarget-q)**2).sum())
    return orientation_error+position_error + w0*((qtarget-q)**2).sum()

robot = rtb.models.URDF.Panda()  # load URDF version of the Panda
print(robot.qlim)

# set the initial configuration
alpha = 0.3
robot.q = 0.3*(robot.qlim[1]-robot.qlim[0])+robot.qlim[0]
print(robot.q)

# set target location
# unreachable pose
# T = SE3(1.0, 1.0, 1.0) * SE3.OA([0, 0, 1], [0,-1,0])
# print(T)
# reachable pose
Tstar = SE3(0.35, 0.0, 0.35) * SE3.OA([0, 0, 1], [0,-1,0])
print(Tstar)

# Set the initial solution for the search
q0=(robot.qlim[1]+robot.qlim[0])/2.0

# print(pose_error_cost(q=robot.q,rob=robot,Tdes=Tstar))

# Define bounds
mybounds = opt.Bounds(robot.qlim[0, :], robot.qlim[1, :])
#Solve
sol = opt.minimize(pose_error_cost,x0=q0,args=(robot,Tstar,1e-03,q0),bounds=mybounds,method='trust-constr',options={'xtol': 1e-08, 'gtol': 1e-08, 'barrier_tol': 1e-08, 'maxiter': 1000, 'verbose': -1,'disp': False})
# print(sol)
# print(sol.x)

#Solve
sol2 = opt.minimize(pose_error_cost,x0=q0,args=(robot,Tstar,0.0,q0),bounds=mybounds,method='trust-constr',options={'xtol': 1e-08, 'gtol': 1e-08, 'barrier_tol': 1e-08, 'maxiter': 1000, 'verbose': -1,'disp': False})
# print(sol)
# print(sol2.x)

# robot._reach = 1.0
# solrob = robot.ikine_min(Tstar, q0=q0, qlim=True, method='trust-constr',ilimit=1000,tol=1e-16)

print(sol.x)
print(robot.fkine(sol.x))
print(pose_error_cost(q=sol.x,rob=robot,Tdes=Tstar,w0=1e-03,qtarget=q0))

print(sol2.x)
print(robot.fkine(sol2.x))
print(pose_error_cost(q=sol2.x,rob=robot,Tdes=Tstar,w0=0.0,qtarget=q0))

# print(robot.fkine(solrob.q))
# print(pose_error_cost(q=solrob.q,rob=robot,Tdes=Tstar))



