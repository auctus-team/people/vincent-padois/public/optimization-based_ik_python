# Optimization-based_IK_Python

Create a new conda environement using the env.yaml configuration file

`conda env create -f env.yaml`

Activate the created conda environment

`conda activate optimIKenv`

Run the provided Python example and enjoy

`python3 ./optimbased_mgi.py`


